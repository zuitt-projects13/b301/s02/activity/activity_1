package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args) {
        int year;
        boolean isLeapYear = false;

        Scanner userInput =new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        year = userInput.nextInt();

        // A leap year is divisible by 4 but not divisible by 100 unless it is also divisible by 400
        if(((year%4) == 0) && ((year%100) != 0) || ((year%400) == 0)) {
            isLeapYear=true;
        }

        if (isLeapYear) {
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is NOT a leap year");
        }
    }
}
